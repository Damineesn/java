package Assesmentjava1;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Employee {
 private int id;
 private String name;
 private int salary;
 private int age;
 private  String dept;


	public Employee( int id,String name,int age,  String dept,int salary) {
		this.id=id;
		this.name=name;
		this.salary=salary;
		this.age=age;
		this.dept=dept;
		
	}
	public int getId()
	{
		return id;
	}
	public void setEmpId(int id) {
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setEmpName(String name) {
		this.name = name;
	}
	public int getAge()
	{
		return age;
	}
	public void setAge(int age)
	{
		this.age=age;
	}
	public int getSalary()
	{
		return salary;
	}
	public void setSalary(int salary)
	{
		this.salary=salary;
	}
	public String getDept()
	{
		return dept;
	}
	public void setDept(String dept)
	{
		this.dept=dept;
	}
	public TreeMap<Integer, Employee> getEmployees() {
		return emp;
	}

	public void setEmployees(TreeMap<Integer, Employee> emp) {
		this.emp = emp;
	}
	@SuppressWarnings("rawtypes")
	Comparator<Employee> EMPLOYEE_SORT_BY_NAME = new Comparator<Employee>() {

		@Override

		public int compare(Employee o1, Employee o2) {

			 if (o1 instanceof Employee && o2 instanceof Employee) {

			 return ((Employee) o1).getName().compareTo(((Employee) o2).getName());

			 }

		return 0;

		}

	};
	TreeMap<Integer, Employee> emp=new TreeMap<>();
	Scanner sc=new Scanner(System.in);
	public void add()
	{
		
		System.out.println("Enter your name:");
		String name=sc.next();
		System.out.println("Enter your Id:");
		int id=sc.nextInt();
		System.out.println("Enter your salary:");
		int salary=sc.nextInt();
		System.out.println("Enter your Age:");
		int age=sc.nextInt();
		System.out.println("Enter your Department:");
		String dept=sc.next();

		Employee empp=new Employee(id,name,age,dept,salary);
		emp.put(empp.getId(),empp);
		
		System.out.println("insert succesfully");
	}
	
	public void print()
	{
		List<Employee> emplSo = new ArrayList<Employee>(emp.values());
		Collections.sort(emplSo, EMPLOYEE_SORT_BY_NAME);

		if (!emplSo.isEmpty()) {
			System.out.format("EmpId \t EmpName \t Age \t Department \t Salary\n");
			for (Employee empp : emplSo) {

				System.out.format(" %d \t %s \t %d \t %s \t %d\n", empp.id, empp.name, 
						empp.age, empp.dept, empp.salary);

			}
		} else {
			System.out.println("Empty");
		}
	}
	@Override
	public String toString() {
		return "EmployeeDetails [empId=" + id + ", empName=" + name + ", age=" + age
				+ ",  department=" + dept + ", salary=" + salary  + "]";
	}
	
	public Employee() {
		
	}
	
	public void delete() {
		int key;
		key=sc.nextInt();
		if(emp.containsKey(key)) {
			emp.remove(key);
		}
		System.out.println("Removed succesfully");
		
		
	}
	public void importDetails()
	{
		try {
			FileOutputStream fileOut = new FileOutputStream("C:\\Users\\DamineeSN\\Employee.txt");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			Object e = null;
			out.writeObject(e);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in C:\\Users\\DamineeSN\\Employee.txt");
		} catch (IOException i) {
			i.printStackTrace();
		}
		
	}
	
	public void exportDetails()
	{
		Employee e = null;
	      try {
	         FileInputStream fileIn = new FileInputStream("C:\\Users\\DamineeSN\\Employee.txt");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         e = (Employee) in.readObject();
	         in.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         System.out.println("Employee class not found");
	         c.printStackTrace();
	         return;
	      }
	      
	      System.out.println("Deserialized Employee...");
	}
	public void stat()
	{
		List<Employee> emplSo = new ArrayList<Employee>(emp.values());
		Collections.sort(emplSo, EMPLOYEE_SORT_BY_NAME);
			int count=0;
			int depnum=0;int depno=0;
			long count1=emplSo.stream().filter(e->e.getAge()>20).count();
			System.out.println("the number of employees age more than 20 using stream:");
			System.out.println(count1);
			List<Integer> abc=new ArrayList<>();
			abc=emplSo.stream().filter(e->e.getAge()>20).map(e->e.getId()).collect(Collectors.toList());
			System.out.println("The Employee id whose age is more than 20:");
			for(Object a:abc)
			{
				System.out.println(a);
			}
			//Map<String, Long> dep=new TreeMap<>();
	Map<String, Long> dep=emp.values()
	.stream()
      .collect(Collectors.groupingBy(Employee :: getDept,Collectors.counting()));
			System.out.println("the department wise employees:");System.out.println(dep);
			//String depa=sc.next();
			
			Map<String, Double> depsort=emp.values()
					.stream()
				      .collect(Collectors.groupingBy(Employee :: getDept,Collectors.averagingInt(Employee::getSalary)));		
			System.out.println("Average Salary:");System.out.println(depsort);
			
			
		//	Map<String, Long> deptgt3 =emp.values()
//					.stream()
//				      .collect(Collectors.groupingBy(Employee :: getDept,Collectors.counting()));
//			List<String> deptgt4=deptgt3.values()
//					.stream().filter(e->e.longValue()>3).map(Collectors.getKey).collect(Collectors.toList());
//			
		if (!emplSo.isEmpty()) {
			System.out.format("EmpId \t EmpName \t Age \t Department \t Salary\n");
		for(Employee emp1:emplSo)
		{
			if(emp1.age>=20)
			{
			count++;
			System.out.format("the age %d \t %s \t %d \t %s \t %d\n", emp1.id, emp1.name, 
					emp1.age, emp1.dept, emp1.salary);
		    }
			
		}System.out.println("the number of employees age more than 20");
		System.out.println(count);
		System.out.println("\n");
		for(Employee emp1:emplSo)
		{
		if(emp1.dept.equals("cse"))
		{
			depnum++;
			System.out.format("dept %d \t %s \t %d \t %s \t %d\n", emp1.id, emp1.name, 
					emp1.age, emp1.dept, emp1.salary);
			
		}}
		
		System.out.println("the number of employees whose department is cse");
		System.out.println(depnum);
		for(Employee emp1:emplSo)
		{
		 if(emp1.dept.equals("ece"))
			{
			 depno++;
			System.out.format("dept %d \t %s \t %d \t %s \t %d\n", emp1.id, emp1.name, 
					emp1.age, emp1.dept, emp1.salary);
			
			}
			}
		System.out.println("the number of employees whose department is eces");
		System.out.println(depno);
		
	}
		else {
			System.out.println("Empty");
		}
	}
	


}

