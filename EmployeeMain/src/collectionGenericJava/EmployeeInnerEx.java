package collectionGenericJava;

public class EmployeeInnerEx {
	
	//1.defining the interface
	public interface ValidateEmployee{
		public boolean check(int age,int salary);
	}
	//2.provide implementation for validate
	 /*public boolean validate(ValidateEmployee validator,int age) {
		return validator.check(age);
	}*/
	boolean validate(ValidateEmployee validator,int age,int salary) {
		return validator.check(age,salary);
	}
	//3.capture info from user
	
	//4.invoking validate method with value captured from user
	//invoke validate method from main after capturing the age
	public static void main(String[] args) {
		int age=0,salary=0;
		EmployeeInnerEx i=new EmployeeInnerEx();
  /* boolean valStatus = i.validate(new ValidateEmployee() {
	                   public boolean check(int age) {
		               return age>0;
	                   }
                       },-10);
   if(!valStatus) {
	   System.out.println("invalid");
   }*/
   boolean valStatus1 =i.validate(new ValidateEmployee() {
       public boolean check(int age,int salary) {
       return age>0 && salary>0;
       }
       },10,1000);
   if(!valStatus1) {
	   System.out.println("invalid");
   }
   else {
	   System.out.println("valid");

   }
	}

}
