package collectionGenericJava;
import java.util.*;
import java.util.function.Predicate;

import collectionGenericJava.Employee;



public class EmployeeValidationMain {

	

	// Validator interface

	// can be qualified as functional interface as it is performing single task

/*	public interface ValidateEmployee {

		public boolean check(Employee<Double> emp);

	}



	// Validate method definition

	public static boolean validate(Employee<Double> emp, ValidateEmployee validator)

	{

		return validator.check(emp);

	}

	

	*/	

	

	public static void validate1(Employee emp, Predicate<Employee> validator)

	{

		if( validator.test(emp))
		{
			System.out.println("Employee saved");// executes lambda expression body
		}
		else
		{
			System.out.println("invalid details");
		}
	}

	

	public static void main(String[] args)

	{

		Employee employee = new Employee(12,"daminee","23","Admin",12000);

		

		// Employee Validatio with Anonymous inner class 

//		boolean valStatus = validate(emp, new ValidateEmployee() {

//			

//			@Override

//			public boolean check(Employee<Double> emp) {

//				return emp.getAge() > 0 && emp.getSalary() > 0 && (emp.getDepartment().equals("Admin") || emp.getDepartment().equals("IT"));

//			}

//		});

		

		 validate1(employee, (Employee e) -> {return ( e.getSalary() > 0 && (e.getDept().equals("Admin") || e.getDept().equals("IT")));});

//		if(valStatus)
//		{
//
//			System.out.println("Employee saved");
//
//	}
//
//		else {
//
//			System.out.println("Invalid Employee details - " + employee);
//
//		}
//
//		

	}
}


	